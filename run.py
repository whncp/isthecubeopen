import RPi.GPIO as GPIO
import time
import sys
import signal
import twitter
import json


GPIO.setmode(GPIO.BOARD) # use board numbering 

DOOR_SENSOR_PIN = 12
GREEN_LIGHT = 3
RED_LIGHT = 5
isClosed = None
prev = None
consumer_key = None
consumer_secret = None
access_token_key = None
access_token_secret = None

with open('config.json') as json_file:
	data = json.load(json_file)
	consumer_key = data["consumer_key"]
	consumer_secret = data["consumer_secret"]
	access_token_key = data["access_token_key"]
	access_token_secret = data["access_token_secret"]

api = twitter.Api(consumer_key= consumer_key,
                  consumer_secret= consumer_secret,
                  access_token_key= access_token_key,
                  access_token_secret= access_token_secret)

GPIO.setup(DOOR_SENSOR_PIN, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(GREEN_LIGHT, GPIO.OUT)
GPIO.setup(RED_LIGHT, GPIO.OUT)

GPIO.output(GREEN_LIGHT, False)
GPIO.output(RED_LIGHT, False)

def cleanupLights(signal, frame): 
    GPIO.output(RED_LIGHT, False) 
    GPIO.output(GREEN_LIGHT, False) 
    GPIO.cleanup() 
    sys.exit(0)

signal.signal(signal.SIGINT, cleanupLights) 

print(api.VerifyCredentials())

while True:
	prev = isClosed
	isClosed = GPIO.input(DOOR_SENSOR_PIN)
	if (isClosed and prev != isClosed):
		print("Door is now closed")
		GPIO.output(GREEN_LIGHT, False) # green light off
		GPIO.output(RED_LIGHT, True) # red light on
	elif (not isClosed and prev != isClosed):
		print("Door is now open")
		GPIO.output(GREEN_LIGHT, True) # green light on
		GPIO.output(RED_LIGHT, False) # red light off
	time.sleep(0.5)

